# Prerequisite
NodeJS: 14.17.0 (especially for Mac M1)

# Run
```sh
$ yarn
$ yarn start
```

# Test
```sh
$ yarn test
```

