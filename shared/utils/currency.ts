import _isUndefined from "lodash/isUndefined";

export const toCents = (
    value: number = 0,
    removedZeroDecimal: boolean = false
): number => {
    const s = value.toString();
    if (s.indexOf(".") > -1) {
        throw new Error("Invalid amount.");
    }
    let sig: string;
    if (value < 100) {
        sig = "0";
    } else {
        sig = s.slice(0, s.length - 2);
    }
    let cents: string;
    if (value % 100 < 10) {
        cents = "0" + (value % 100);
    } else {
        cents = (value % 100).toString();
    }

    let res = `${sig}.${cents}`;
    if (removedZeroDecimal) {
        if (res.length > 3 && res.slice(-3) === ".00") {
            res = res.substr(0, res.length - 3);
        }
    }

    return Number(res);
};

export const amountStringToInt = (amount: string | number): number => {
    amount = amount.toString();
    const parts = amount.split(".");
    if (parts.length > 2) {
        throw new Error("Invalid amount.");
    }

    const [significant, maybeCents] = parts;
    let cents = maybeCents;
    if (_isUndefined(cents)) {
        cents = "00";
    }
    if (cents.length === 1) {
        cents = cents + "0";
    }
    if (cents.length > 2) {
        throw new Error("Cents is at most 2 dp.");
    }

    const n = Number(significant.split(",").join("") + cents);
    if (isNaN(n)) {
        throw new Error(`That's not a valid amount.`);
    }
    return n;
}