import test from "ava";
import {toCents, amountStringToInt} from "./currency";

test("toCents()", t => {
    t.is(toCents(0, true), 0)
    t.is(toCents(100, true), 1)
    t.is(toCents(111, true), 1.11)
    t.is(toCents(14, true), 0.14)
    t.is(toCents(1400, true), 14)
    t.is(toCents(1489, true), 14.89)
    t.is(toCents(14890, true), 148.9)
    t.is(toCents(14899, true), 148.99)
    t.throws(() => {
        toCents(148.99, true)
    }, {instanceOf: Error})
    t.is(toCents(148997, true), 1489.97)
})

test("amountStringToInt()", t => {
    t.is(toCents(), 0)

    t.is(amountStringToInt(0), 0)
    t.is(amountStringToInt(0.00), 0)
    t.is(amountStringToInt(0.01), 1)
    t.is(amountStringToInt(0.91), 91)
    t.is(amountStringToInt(1.91), 191)
    t.is(amountStringToInt(10.90), 1090)
    t.throws(() => {
        amountStringToInt(10.901)
    }, { instanceOf: Error })
    t.is(amountStringToInt(10.00), 1000)

    t.is(amountStringToInt("0"), 0)
    t.is(amountStringToInt("0.00"), 0)
    t.is(amountStringToInt("0.01"), 1)
    t.is(amountStringToInt("0.91"), 91)
    t.is(amountStringToInt("1.91"), 191)
    t.is(amountStringToInt("10.90"), 1090)
    t.throws(() => {
        amountStringToInt("10.901")
    }, { instanceOf: Error })
    t.is(amountStringToInt("10.00"), 1000)

    t.throws(() => {
        amountStringToInt("0.0.0")
    }, { instanceOf: Error })

    t.throws(() => {
        amountStringToInt("ddd")
    }, { instanceOf: Error })
})
