export const sampleERSource = [
    {
        id: 1,
        name: "e-Rate",
        code: "e_rate",
    },
    {
        id: 2,
        name: "TT Counter",
        code: "tt_counter",
    },
    {
        id: 3,
        name: "Bank Notes",
        code: "bank_notes",
    },
]