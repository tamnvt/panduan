import {
    rListExchangeRateSources,
    rUpdateExchangeRate,
} from "../repo/rExchangeRate";
import {PErrorVars} from "../error/var"
import {PError} from "../error/panduan_error"
import _find from "lodash/find"
import {amountStringToInt} from "../../../utils/currency";
import {ExchangeRateParams, validateExchangeRateObject} from "./utils";
import {PoolConnection} from "mysql";
import {dbCommit, dbRollback, getConnection} from "../../../config/db";

export const sUpdateExchangeRate = async (
    exchangeRate: ExchangeRateParams,
): Promise<{
    [sourceCode: string]: {
        "jual"?: number, // sell, in decimal
        "beli"?: number  // buy, in decimal
    }
} | undefined > => {

    const erSources = await rListExchangeRateSources(global.db)

    const errMap = validateExchangeRateObject(exchangeRate, erSources)
    if (Object.keys(errMap).length > 0) {
        throw new PError(PErrorVars.E100_MULTIPLE_ERRORS, "LOGIC", errMap)
    }

    const updatedSources: {
        [sourceCode: string]: {
            "jual"?: number, // sell, in decimal
            "beli"?: number  // buy, in decimal
        }
    } = {}

    const {symbol, date, ...sources} = exchangeRate
    const pool = await getConnection(global.db)

    try {
        for (const sourceCode in sources) {
            if (sources.hasOwnProperty(sourceCode)) {
                const currencyId = symbol.toUpperCase()

                try {
                    const affectedRows = await rUpdateExchangeRate(pool,
                        currencyId,
                        (_find(erSources, {code: sourceCode}) as any).id, // cant be undefined for sure. checked above alr. So we can bypass typescript by using "any"
                        amountStringToInt(sources[sourceCode].beli as number),
                        amountStringToInt(sources[sourceCode].jual as number),
                        date)
                    if (!affectedRows || affectedRows === 0) {
                        continue
                    }
                } catch (e) {
                    // Do nothing
                }
                updatedSources[sourceCode] = sources[sourceCode]
            }
        }

        if (Object.keys(updatedSources).length === 0) {
            throw new PError(PErrorVars.E105_NO_AFFECTED, "LOGIC")
        }

        await dbCommit(pool)

        return updatedSources
    } catch (e) {
        await dbRollback(pool)
        throw e
    } finally {
        pool.release()
    }

}