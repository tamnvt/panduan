import {AsyncReturnType} from "../../../types";
import {rListExchangeRateSources} from "../repo/rExchangeRate";
import {PErrorVars} from "../error/var";
import moment from "moment";
import _find from "lodash/find";

export type ExchangeRateParams = {
    [sourceCode: string]: {
        "jual": any, // sell, in decimal
        "beli": any  // buy, in decimal
    }
} & {
    symbol?: any, // currency iso code: SGD
    date?: any // YYYY-MM-DD
}

export const validateExchangeRateObject = (
    exchangeRate: ExchangeRateParams,
    erSources: AsyncReturnType<typeof rListExchangeRateSources>
): {
    [key: string]: string,
} => {

    const errMap: {
        [key: string]: string,
    } = {}

    const {symbol, date, ...sources} = exchangeRate

    if (typeof sources !== "object" || Object.keys(sources).length === 0) {
        errMap["sources"] = PErrorVars.E107_MISSING_SOURCE
    }
    if (!symbol || typeof symbol !== "string" || symbol.length > 3) {
        errMap["symbol"] = PErrorVars.E102_INVALID_CURRENCY
    }
    if (!validateDate(date)) {
        errMap["date"] = PErrorVars.E101_INVALID_DATE
    }

    for (const sourceCode in sources) {
        if (sources.hasOwnProperty(sourceCode)) {
            if (_find(erSources, {code: sourceCode}) === undefined) {
                errMap[`${sourceCode}`] = PErrorVars.E104_INVALID_SOURCE_CODE
            } else {
                if (typeof sources[sourceCode].jual !== "number" || sources[sourceCode].jual === undefined || isNaN(sources[sourceCode].jual as number)) {
                    errMap[`${sourceCode}.jual`] = PErrorVars.E103_INVALID_NUMBER
                }
                if (typeof sources[sourceCode].beli !== "number" || sources[sourceCode].beli === undefined || isNaN(sources[sourceCode].beli as number)) {
                    errMap[`${sourceCode}.beli`] = PErrorVars.E103_INVALID_NUMBER
                }
            }
        }
    }

    return errMap
}

export const validateDate = (date: any): boolean => {
    if (!date || typeof date !== "string") {
        return false
    } else if (!(/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/.test(date)) ||
        !moment(date, "YYYY-MM-DD").isValid()) {
        return false
    }

    return true
}