import {
    rDeleteExchangeRatesByDate,
    rArchiveExchangeRatesByDate,
    acquireExchangeRateLock, releaseExchangeRateLock
} from "../repo/rExchangeRate";
import {PErrorVars} from "../error/var"
import {PError} from "../error/panduan_error"
import moment from "moment"
import {validateDate} from "./utils";
import {PoolConnection} from "mysql";
import {dbBegin, dbCommit, dbRollback, getConnection} from "../../../config/db";

export async function sDeleteExchangeRateByDate(dateStr?: string) {

    if (!validateDate(dateStr)) {
        throw new PError(PErrorVars.E101_INVALID_DATE, "LOGIC")
    }

    const dateMoment = moment(dateStr, "YYYY-MM-DD")

    const pool = await getConnection(global.db)

    try {
        await dbBegin(pool)
        const locked = await acquireExchangeRateLock(pool)
        if (locked !== 1) {
            throw new PError(PErrorVars.E002_LOCKED_UPDATE_EXCHANGE_RATE, "LOGIC")
        }
        await rArchiveExchangeRatesByDate(pool, dateMoment.format("YYYY-MM-DD"))
        await rDeleteExchangeRatesByDate(pool, dateMoment.format("YYYY-MM-DD"))
        await dbCommit(pool)
    } catch (e) {
        await dbRollback(pool)
        throw e
    } finally {
        await releaseExchangeRateLock(pool)
        pool.release()
    }
}