import {doHttp} from "../../../utils/httpClient";
import cheerio from "cheerio"
import {
    acquireExchangeRateLock,
    releaseExchangeRateLock,
    rInsertExchangeRate,
    rListExchangeRateSources
} from "../repo/rExchangeRate";
import {amountStringToInt} from "../../../utils/currency";
import moment from "moment"
import {PError} from "../error/panduan_error";
import {PErrorVars} from "../error/var";
import {dbBegin, dbCommit, dbRollback, getConnection} from "../../../config/db";
import {CrawledTarget} from "../const";

export async function sIndexExchangeRate() {
    const pool = await getConnection(global.db)

    try {
        const nowDateStr = moment().format("YYYY-MM-DD")
        const exchangeRates = await crawlKur(CrawledTarget.informasi)

        await dbBegin(pool)

        const locked = await acquireExchangeRateLock(pool)
        if (locked !== 1) {
            throw new PError(PErrorVars.E002_LOCKED_UPDATE_EXCHANGE_RATE, "LOGIC")
        }
        for (const group in exchangeRates) {
            if (exchangeRates.hasOwnProperty(group)) {
                const [currencyId, sourceId] = group.split("-")
                await rInsertExchangeRate(global.db,
                    currencyId, parseInt(sourceId, 10,),
                    exchangeRates[group].buy,
                    exchangeRates[group].sell,
                    nowDateStr)
            }
        }

        await dbCommit(pool)

    } catch (e) {
        await dbRollback(pool)
        throw e
    } finally {
        await releaseExchangeRateLock(pool)
        pool.release()
    }
}

type CrawledExchangeRates = {
    [group: string]: { // group format: [currencyId-sourceId]
        buy: number,
        sell: number,
    }
}

const crawlKur = async (targetId: CrawledTarget): Promise<CrawledExchangeRates> => {
    switch (targetId) {
        case CrawledTarget.informasi:
            return await crawlKurInformasi();

        default:
            return {}
    }
}

const crawlKurInformasi = async (): Promise<CrawledExchangeRates> => {
    const erSources = await rListExchangeRateSources(global.db)

    const res = await doHttp({
        serviceName: "crawl_exchange_rate_informasi"
    })
    if (res.status !== 200) {
        throw new PError("UNCAUGHT_ERROR", "INTEGRATION")
    }
    const resText = await res.text()
    const $ = cheerio.load(resText);

    // mapping table column in html to source map like
    const colToSourceMap: {
        currencyCol: number, // Currency column
        sourceCols: Array<{  // sources
            colIndex: number,    // relative column in table html
            sourceId: number,    // sourceId is converted from source label in html
        }>
    } = {
        currencyCol: -1,
        sourceCols: []
    }

    // 1. Check if buy column comes first or sell.
    let buyFirst: boolean | undefined;
    $('.m-table-kurs--sticky table thead tr').each(function (trI, tr) {
        if (trI !== 1) {
            // Do nothing
            return;
        }

        $(tr).find("td").each(function (tdI, td){
            if (buyFirst === undefined) {
                if ($(td).text().trim().toLowerCase() === "buy") {
                    buyFirst = true
                }
                if ($(td).text().trim().toLowerCase() === "sell") {
                    buyFirst = false
                }
            }
        })

    })

    if (buyFirst === undefined) {
        // Not sure what comes first means the provider changed his template. Be careful.
        throw new PError(PErrorVars.E108_CRAWLED_TEMPLATE_CHANGED)
    }

    // 2. Loop through table header to find which column contain which information
    let colIndex = 0 // iterable index
    $('.m-table-kurs--sticky table thead tr th').each(function (thI, th) {
        const thText = $(th).text().toLowerCase()
        if (thText.indexOf("currency") > -1) {
            // Yo, found currency column
            colToSourceMap.currencyCol = colIndex
        } else {
            for (const s of erSources) {
                if (thText.indexOf(s.name.toLowerCase()) > -1) {
                    // Yo, found source respectively in database
                    colToSourceMap.sourceCols.push({
                        colIndex,
                        sourceId: s.id
                    })
                    // Note: for those sources were not found in database, we skip them.
                }
            }
        }

        const colspan = parseInt($(th).attr("colspan") || "1", 10)
        colIndex += colspan
    })

    // 3.
    // Now we know exactly which column in table body containing which information, feel free to get content using
    // td:nth-child or sth sufficient.
    // Nice, let's convert to exchange rates
    const exchangeRates: CrawledExchangeRates  = {}
    $('.m-table-kurs--sticky table tbody tr').each(function (trI, tr) {
        const currentCurrency = $($(tr).children().get(colToSourceMap.currencyCol)).text().trim().toUpperCase()
        for (const col of colToSourceMap.sourceCols) {
            const value = $($(tr).children().get(col.colIndex)).text().trim().replace(/,/, "")
            const nextColValue = $($(tr).children().get(col.colIndex + 1)).text().trim().replace(/,/, "")
            if (buyFirst) {
                exchangeRates[`${currentCurrency}-${col.sourceId}`] = {
                    buy: amountStringToInt(value),
                    sell: amountStringToInt(nextColValue),
                }
            } else {
                exchangeRates[`${currentCurrency}-${col.sourceId}`] = {
                    sell: amountStringToInt(value),
                    buy: amountStringToInt(nextColValue),
                }
            }
        }
    })

    if (Object.keys(exchangeRates).length === 0) {
        throw new PError(PErrorVars.E106_UNEXPECTED_RESULT)
    }

    return exchangeRates
}