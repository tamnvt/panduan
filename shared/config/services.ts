import {Services} from "../types/services";
import exp from "constants";

const sharedServices: Services =  [
    {
        base_url: "https://www.bca.co.id/en",
        name: "Kurs Service",
        services: {
            crawl_exchange_rate_informasi: "GET /informasi/kurs",
            sample_service: "GET /sample/:param_name",
        },
    }

    // Others services.
    // {...}
]

export default sharedServices