import {Pool, PoolConnection} from "mysql";
import {promisify} from "util";

const pool = require("mysql").createPool({
    connectionLimit : 10,
    port            : 3306,
    host            : process.env.DATABASE_HOST,
    user            : process.env.DATABASE_USER,
    password        : process.env.DATABASE_PASSWORD,
    database        : process.env.DATABASE_NAME,

    multipleStatements: true
})

export const dbBegin = (db: PoolConnection) => {
    return new Promise(function (resolve, reject){
        db.beginTransaction(function (err){
          if (err) {
              reject(err)
          } else {
              resolve(true)
          }
        })
    })
}
export const dbCommit = (tx: PoolConnection) => {
    return new Promise(function (resolve, reject){
        tx.commit(function (err){
            if (err) {
                reject(err)
            } else {
                resolve(true)
            }
        })
    })
}
export const dbRollback = (tx?: PoolConnection) => {
    if (tx === undefined) {
        return
    }
    return new Promise(function (resolve, reject){
        tx.rollback(function (err){
            if (err) {
                reject(err)
            } else {
                resolve(true)
            }
        })
    })
}
export const getConnection = (db: Pool): Promise<PoolConnection> => {
    return new Promise(function (resolve, reject){
        db.getConnection(function (err, tx){
            if (err) {
                reject(err)
            } else {
                resolve(tx)
            }
        })
    })
}
export default pool
