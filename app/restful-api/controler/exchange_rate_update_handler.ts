import express from "express";
import {sUpdateExchangeRate} from "../../../shared/domain/core/service/sUpdateExchangeRate"
import {responseError} from "./utils"
import {PError} from "../../../shared/domain/core/error/panduan_error";
import {PErrorVars} from "../../../shared/domain/core/error/var";
import {ExchangeRateParams} from "../../../shared/domain/core/service/utils";

export const exchangeRateUpdateHandler = async (
  req: express.Request<{}, {}, ExchangeRateParams>,
  res: express.Response
) => {

    try {
        const added = await sUpdateExchangeRate(req.body)
        res.status(200).json(added)
    } catch(e) {
        if (e instanceof PError && e.message === PErrorVars.E105_NO_AFFECTED) {
            res.sendStatus(404)
            return
        }
        responseError(e, res)
    }
};
