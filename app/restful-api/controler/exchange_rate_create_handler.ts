import express from "express";
import {sCreateExchangeRate} from "../../../shared/domain/core/service/sCreateExchangeRate"
import {responseError} from "./utils"
import {ExchangeRateParams} from "../../../shared/domain/core/service/utils";

export const exchangeRateCreateHandler = async (
  req: express.Request<{}, {}, ExchangeRateParams>,
  res: express.Response
) => {
    try {
        const added = await sCreateExchangeRate(req.body)
        res.status(200).json(added)
    } catch(e) {
        responseError(e, res)
    }
};
