import test from 'ava';
import app from "./index"
import request from "supertest"
import sinon from "sinon"
import {cleanupDB} from "../../shared/domain/core/repo/cleanup";
import * as httpClient from "../../shared/utils/httpClient";
import {Response} from "node-fetch";
import moment from "moment";
import {PErrorVars} from "../../shared/domain/core/error/var";

const today = moment().format("YYYY-MM-DD")
const yesterday = moment().subtract(1, 'day').format("YYYY-MM-DD")
const twoDaysAgo = moment().subtract(2, 'day').format("YYYY-MM-DD")

test.before("Cleanup DB", async t => {
    await cleanupDB()
})

let sandbox: sinon.SinonSandbox;
test.beforeEach(t => {
    sandbox = sinon.createSandbox()
})

test.afterEach(t => {
    sandbox.restore()
})

/**
 * GET /api/indexing
 * =======================================================
 */
test('GET /api/indexing first time', async t => {
    await request(app)
        .get('/api/indexing')
        .expect(200)
        .then((res) => {
            t.pass()
        })
})

test('GET /api/indexing second time', async t => {
    await request(app)
        .get('/api/indexing')
        .expect(200)
        .then((res) => {
            t.pass()
        })
})

test.failing('GET /api/indexing with incorrect html format', async t => {
    const params = {
        serviceName: "crawl_exchange_rate_informasi"
    }
    const stub = sandbox.stub(httpClient, "doHttp")
    const res = new Response(undefined, {
        status: 200
    })
    res.text = () => Promise.resolve("Invalid html")

    stub.withArgs(params).returns(Promise.resolve(res))
    await request(app)
        .get('/api/indexing')
        .expect(500)
        .then((res) => {
            t.fail()
        })
})

test.failing('GET /api/indexing with request failed', async t => {
    const params = {
        serviceName: "crawl_exchange_rate_informasi"
    }
    const stub = sandbox.stub(httpClient, "doHttp")
    const res = new Response(undefined, {
        status: 503
    })

    stub.withArgs(params).returns(Promise.resolve(res))
    await request(app)
        .get('/api/indexing')
        .expect(500)
        .then((res) => {
            t.fail()
        })
})

/**
 * GET /api/kurs
 * =======================================================
 */
test('GET /api/kurs', async t => {
    await request(app)
        .get('/api/kurs')
        .expect(200)
        .expect(res => {
            t.true(res.body.data.length > 0)
        })
        .then((res) => {
            t.pass()
        })
})

test(`GET /api/kurs?startdate=${yesterday}&enddate=${today}`, async t => {
    await request(app)
        .get(`/api/kurs?startdate=${yesterday}&enddate=${today}`)
        .expect(200)
        .expect(res => {
            t.true(res.body.data.length > 0)
        })
        .then((res) => {
            t.pass()
        })
})

test(`/api/kurs?startdate=${today}&enddate=${yesterday}`, async t => {
    await request(app)
        .get(`/api/kurs?startdate=${today}&enddate=${yesterday}`)
        .expect(200)
        .expect(res => {
            t.true(res.body.data.length === 0)
        })
        .then((res) => {
            t.pass()
        })
})

test.failing(`/api/kurs?startdate=invalid-date&enddate=${today}`, async t => {
    await request(app)
        .get(`/api/kurs?startdate=invalid-date&enddate=${today}`)
        .expect(422, {
            code: PErrorVars.E100_MULTIPLE_ERRORS,
            fields: {
                "startddate": PErrorVars.E101_INVALID_DATE,
            }
        })
        .then(res => {
            t.fail()
        })
        .catch(() => {
            t.pass()
        })
})

test.failing(`/api/kurs?startdate=${today}&enddate=invalid-date`, async t => {
    await request(app)
        .get(`/api/kurs?startdate=${today}&enddate=invalid-date`)
        .expect(422, {
            code: PErrorVars.E100_MULTIPLE_ERRORS,
            fields: {
                "endddate": PErrorVars.E101_INVALID_DATE,
            }
        })
        .then(res => {
            t.fail()
        })
        .catch(() => {
            t.pass()
        })
})

/**
 * GET /api/kurs
 * =======================================================
 */
test('GET /api/kurs/SGD', async t => {
    await request(app)
        .get('/api/kurs/SGD')
        .expect(200)
        .expect(res => {
            t.true(res.body.data.length > 0)
        })
        .then((res) => {
            t.pass()
        })
})

test('GET /api/kurs/SGD1', async t => {
    await request(app)
        .get('/api/kurs/SGD1')
        .expect(200)
        .expect(res => {
            t.true(res.body.data.length === 0)
        })
        .then((res) => {
            t.pass()
        })
})

/**
 * POST /api/kurs
 * =======================================================
 */

test('POST /api/kurs', async t => {
    await request(app)
        .post('/api/kurs')
        .send({
                "symbol": "SGD",
                "e_rate": {
                    "jual": 1803.55,
                    "beli": 177355
                },
                "tt_counter": {
                    "jual": 1803.55,
                    "beli": 1773.55
                },
                "bank_notes": {
                    "jual": 1803.55,
                    "beli": 177355
                },
                "date": "2021-05-17"
            }
        )
        .expect(200, {
            "e_rate": {
                "jual": 1803.55,
                "beli": 177355
            },
            "tt_counter": {
                "jual": 1803.55,
                "beli": 1773.55
            },
            "bank_notes": {
                "jual": 1803.55,
                "beli": 177355
            },
        })
        .then((res) => {
            t.pass()
        })
})

test('POST /api/kurs duplicate', async t => {
    await request(app)
        .post('/api/kurs')
        .send({
                "symbol": "SGD",
                "e_rate": {
                    "jual": 1803.55,
                    "beli": 177355
                },
                "tt_counter": {
                    "jual": 1803.55,
                    "beli": 1773.55
                },
                "bank_notes": {
                    "jual": 1803.55,
                    "beli": 177355
                },
                "date": "2021-05-17"
            }
        )
        .expect(200, {})
        .then((res) => {
            t.pass()
        })
})

test.failing('POST /api/kurs with incorrect payload', async t => {
    await request(app)
        .post('/api/kurs')
        .send({
                "symbol": "SGD",
                "e_rate": {
                    "jual": "1803.55",
                    "beli": "dddd",
                },
                "date": "2021-05-17ddd"
            }
        )
        .expect(422, {
            "code": "E100_MULTIPLE_ERRORS",
            "fields": {
                "date": "E001_INVALID_DATE",
                "e_rate.jual": "E103_INVALID_NUMBER",
                "e_rate.beli": "E103_INVALID_NUMBER",
            },
        })
        .then((res) => {
            t.fail()
        }).catch(e => {
            t.pass()
        })
})

test.failing('POST /api/kurs with invalid currency', async t => {
    await request(app)
        .post('/api/kurs')
        .send({
                "symbol": "SGD1",
                "e_rate": {
                    "jual": 1803.55,
                    "beli": 1803.55,
                },
                "date": "2021-05-17"
            }
        )
        .expect(422, {
            "code": "E100_MULTIPLE_ERRORS",
            "fields": {
                "symbol": "E102_INVALID_CURRENCY",
            },
        })
        .then((res) => {
            t.fail()
        }).catch(e => {
            t.pass()
        })
})

test.failing('POST /api/kurs with invalid currency 2', async t => {
    await request(app)
        .post('/api/kurs')
        .send({
                "symbol": "000",
                "e_rate": {
                    "jual": 1803.55,
                    "beli": 1803.55,
                },
                "date": "2021-05-17"
            }
        )
        .expect(500)
        .then((res) => {
            t.fail()
        }).catch(e => {
            t.pass()
        })
})

/**
 * PUT /api/kurs
 * =======================================================
 */
test('PUT /api/kurs', async t => {
    await request(app)
        .put('/api/kurs')
        .send({
                "symbol": "SGD",
                "e_rate": {
                    "jual": 1804.55,
                    "beli": 1804.55,
                },
                "date": "2021-05-17"
            }
        )
        .expect(200, {
            "e_rate": {
                "jual": 1804.55,
                "beli": 1804.55,
            },
        })
        .then((res) => {
            t.pass()
        })
})

test.failing('PUT /api/kurs with incorrect payload', async t => {
    await request(app)
        .put('/api/kurs')
        .send({
            "symbol": "SGD",
            "e_rate": {
                "jual": "1803.55",
                "beli": "dddd",
            },
            "date": "2021-05-17ddd"
        })
        .expect(422, {
            "code": "E100_MULTIPLE_ERRORS",
            "fields": {
                "date": "E001_INVALID_DATE",
                "e_rate.jual": "E103_INVALID_NUMBER",
                "e_rate.beli": "E103_INVALID_NUMBER",
            },
        })
        .then((res) => {
            t.fail()
        }).catch(e => {
            t.pass()
        })
})

test.failing('PUT /api/kurs with invalid currency', async t => {
    await request(app)
        .put('/api/kurs')
        .send({
                "symbol": "SGD1",
                "e_rate": {
                    "jual": 1803.55,
                    "beli": 1803.55,
                },
                "date": "2021-05-17"
            }
        )
        .expect(422, {
            "code": "E100_MULTIPLE_ERRORS",
            "fields": {
                "symbol": "E102_INVALID_CURRENCY",
            },
        })
        .then((res) => {
            t.fail()
        }).catch(e => {
            t.pass()
        })
})

test.failing('PUT /api/kurs with currency not found', async t => {
    await request(app)
        .put('/api/kurs')
        .send({
                "symbol": "000",
                "e_rate": {
                    "jual": 1803.55,
                    "beli": 1803.55,
                },
                "date": "2021-05-17"
            }
        )
        .expect(404)
        .then((res) => {
            t.fail()
        }).catch(e => {
            t.pass()
        })
})

/**
 * DELETE /api/kurs/:date
 * =======================================================
 */

test(`DELETE /api/kurs/${today}`, async t => {
    await request(app)
        .del(`/api/kurs/${today}`)
        .expect(200)
        .then((res) => {
            return new Promise(async (resolve, reject) => {
                try {
                    await request(app)
                        .get(`/api/kurs?startdate=${today}&enddate=${today}`)
                        .expect(200)
                        .expect(res => {
                            t.true(res.body.data.length === 0)
                        })
                        .then((res) => {
                            resolve(true)
                        })
                        .catch(e => {
                            throw e
                        })
                } catch (e) {
                    t.fail()
                }
            })
        })
        .then(() => {
            t.pass()
        })
})

test.failing(`DELETE /api/kurs/`, async t => {
    await request(app)
        .del(`/api/kurs/`)
        .expect(404)
        .then(() => {
            t.fail()
        })
        .catch(e => {
            t.pass()
        })
})

test.failing(`DELETE /api/kurs/invalid-date`, async t => {
    await request(app)
        .del(`/api/kurs/invalid-date`)
        .expect(422, {
            "code": PErrorVars.E101_INVALID_DATE,
        })
        .then(() => {
            t.fail()
        })
        .catch(e => {
            t.pass()
        })
})

test.failing(`DELETE /api/kurs/2021-20-20`, async t => {
    await request(app)
        .del(`/api/kurs/2021-20-20`)
        .expect(422, {
            "code": PErrorVars.E101_INVALID_DATE,
        })
        .then(() => {
            t.fail()
        })
        .catch(e => {
            t.pass()
        })
})

test(`DELETE /api/kurs/${twoDaysAgo}`, async t => {
    await request(app)
        .del(`/api/kurs/${twoDaysAgo}`)
        .expect(200)
        .then(() => {
            t.pass("No record to delete, but this should be marked as success")
        })
})